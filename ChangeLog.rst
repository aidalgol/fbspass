=========
ChangeLog
=========
All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_ and this project adheres to `Semantic Versioning`_.

Unreleased
==========

1.0.0
=====
Changed
-------
- Fix selector for post subtitles.

0.1.3
=====
Changed
-------
- Fix manifest.

0.1.2
=====
Changed
-------
- Relicensed under the GPL.
- Remove logging to browser console.

0.1.1
=====
Changed
-------
- Remove non-source project files from add-on build bundle.

0.1
===
Initial release.

_Keep a Changelog: https://keepachangelog.com/en/1.0.0/
_Semantic Versioning: https://semver.org/spec/v2.0.0.html
