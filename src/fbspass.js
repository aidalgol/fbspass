// This file is part of FB Sponsored Post Assassin
// Copyright (C) 2019  Aidan Gauland
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

"use strict";

const FEED_SUBTITLE_SELECTOR = "[id*='fb-story']";

function stripElementText(element) {
  return element.textContent.replace(/[^A-z]+/gi, "");
}

function isFeedPostContainer(element) {
  return element.id.match(/^hyperfeed_story/);
}

function isSubstreamElement(element) {
  return element.id.match(/^substream/);
}

// feedElement may be either the top-level feed element or a substream element.
function findAndHideSponsoredPosts(feedElement) {
  const subtitles = feedElement.querySelectorAll(FEED_SUBTITLE_SELECTOR);
  subtitles.forEach(
    (elt) => {
      if (stripElementText(elt) === "Sponsored") {
        let parent = elt.parentNode;
        while (!isFeedPostContainer(parent)) {
          parent = parent.parentNode;
        }
        parent.style.display = "none";
      }
    }
  );
}

// Initial post hiding.
const feedElement = document.querySelector("[role='feed']");
findAndHideSponsoredPosts(feedElement);

// Set up listener for more posts.
let substreamObservers = [];

function onSubstreamSubtreeChange(mutations) {
  for(let mutation of mutations) {
    findAndHideSponsoredPosts(mutation.target);
  }
}

function onSubstreamsAdded(mutations) {
  // Clear previous substream subtree observers.
  substreamObservers.forEach(
    (item) => {
      item.disconnect();
    }
  );
  substreamObservers = [];
  for(let mutation of mutations) {
    mutation.addedNodes.forEach(
      (elt) => {
        if (isSubstreamElement(elt)) {
          // Scan the substreams, but also attach subtree observers to them to
          // handle delayed loading of the posts.
          findAndHideSponsoredPosts(elt);
          const aSubstreamObserver = new MutationObserver(onSubstreamSubtreeChange);
          // The subtree option augments the required options, so we must
          // specify subtree in addition to childList, not instead of.
          aSubstreamObserver.observe(elt, {childList: true, subtree: true});
          substreamObservers.push(aSubstreamObserver);
        }
      }
    );
  }
}

const morePagerObserver = new MutationObserver(onSubstreamsAdded);
// It it actually the first child of the "more_pager_pagelet" element is
// actualthe one to which "substream" elements are added.
const morePagerElement = feedElement.querySelector("[id*='more_pager_pagelet']");
morePagerObserver.observe(morePagerElement.children[0], {childList: true});
