================================
Facebook Sponsored Post Assassin
================================
A more aggressive Facebook Ad blocker.

Facebook obfuscates "sponsored" posts, so that a single CSS selector cannot match them, thereby thwarting most ad blockers.  fbspass searches for these posts more aggressively to circumvent this obfuscation.

Methodology
===========
We determine that a post is a "sponsored" post (an ad) if it has the subtitle of "Sponsored" rendered.  Facebook tries to make this difficult to search for by

a. Wrapping each character in the subtitle in an element (twice), and
b. Making some of these elements hidden.

Together, these serve to make the `textContent` attribute of the DOM element for the subtitle fail to match a string search for "Sponsored" in the DOM tree, because the text is really, for example, `"----S-p--o---n--sor--e---d-- · "`.  (Note that a. does not need to be done twice, but Facebook's DOM is already full of needlessly nested elements, so why not here as well.)

To get around this, we first search for the subtitle element, then get the `textContent` string and strip all non-alphabetic characters.  If this string is "Sponsored", then we consider it a sponsored post and then hide it by walking up the DOM until we find an element with an `id` of `hyperfeed_story`, and then set the CSS attribute `display` to `none`.  (Removing the element would be less performant than simply hiding it.)

More Posts
----------
This is not enough, because we now have to handle Facebook's infinite scrolling.  Posts loaded after the initial page load seem to always go into the first element with an `id` starting with `more_pager_pagelet`, but in chunks, so that new posts are added to this element several at a time inside a "substream" element (it has an id starting with `substream`).  So we attatch a MutationObserver_ to the `more_pager_pagelet` element (actually the first child element, because Facebook pointless wrapper elements everywhere) and scan new "substream" elements for sponsored posts.

There is one more catch here: the "substream" elements do not contain the fully-populated posts immediately upon being added to the DOM.  There appears to be a delay, possibly from asynchronous loading of the posts' content, so we need to somehow wait until they are fully loaded before scanning.  To handle this, we attach a full subtree-change observer to each substream element, and rerun the search on every change.  When the next chunk of substreams is added, we clear the observers for the last batch, because when new posts are loaded, all earlier posts in the feed should be fully loaded and rendered.

.. _MutationObserver: https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
